# Lite News Utils

While the content of the news you read might be overwhelming, the medium that
you read it on should be as distracting free as possible. This web extension aims to providing utilitities and a unified style for minimilist news sites.

# Installation

## Firefox

https://addons.mozilla.org/en-US/firefox/addon/text-npr-to-full-version/

## Other browsers

Coming soon! (I'm just a little bit lazy :P)

# Minimalist News Sites

- https://text.npr.org *
- https://lite.cnn.io &
- https://legiblenews.com

> \* currently supported by this extension

> & planned support

# Features

## https://text.npr.org: Link to Full Content

The "full version" in articles on the https://text.npr.org site links back to the main NPR page, and not the article.
https://text.npr.org is text-only. Pictures (for photo-articles), podcasts, videos, and other forms of media will not be shown, so clicking "full version" is sometimes necessary. This makes the "full version" link go to the full version article.

![Shows a comparison between a text.npr.org article in two browsers, one with the extension and one without. The one with the extension goes to the full version of the article when 'full version' is clicked, while the other goes to the default npr site](feature/text.npr.org-full-version-link.gif)

> Shows a comparison between a text.npr.org article in two browsers, one with the extension and one without. The one with the extension goes to the full version of the article when 'full version' is clicked, while the other goes to the default npr site

## https://text.npr.org: H1/H2 Tags for Title and Author

Shows a comparison between a text.npr.org article in two browsers, the left with
the extension and right without. The left plugin and puts the article title in
the proper HTML tags. The side without the plugin renders it as paragraph text

![shows a side by side of the text npr website. one side uses the plugin and has
the article title using the proper html tags. the side without the plugin
renders it as paragraph text.](feature/title.png)

# Contribution

If you know of any minimalist news sites, _please_ submit a pull request adding
it to the readme ☺️. If you want to submit a pull request but don't know how,
create a new
[issue](https://github.com/isthisnagee/lite-news-utils/issues) and mention that
you need some help with a pull request. If you want to add to the list but don't
want to submit a pull request/register for github/etc, you can send me an email
at isthisnagee+lite@gmail.com (yes, with the plus sign), or you can DM me on
twitter [@isthisnagee](https://twitter.com/isthisnagee)). You can also email or
DM me if you have questions about pull requests.



