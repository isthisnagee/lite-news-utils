const replaceTag = (parent, item, newTag) => {
  const newNode = document.createElement(newTag);
  newNode.innerHTML = item.innerHTML;
  parent.insertBefore(newNode, item);
  item.remove();
};

const body = document.body;
const fullVersionLink = document.querySelector('a[href="https://www.npr.org"]');
const url = new URL(window.location.href);
const id = url.searchParams.get('sId');

if (fullVersionLink.text === 'full version' && id) {
  fullVersionLink.setAttribute('href', `https://npr.org/${id}`);
}

const ps = document.querySelectorAll('p');
if (ps.length > 4) {
  const maybeHeader = ps[2];
  const maybeAuthor = ps[3];
  if (maybeAuthor.textContent.toLowerCase().indexOf('by') > -1) {
    replaceTag(body, maybeHeader, 'h1');
    replaceTag(body, maybeAuthor, 'h2');
  }
}
